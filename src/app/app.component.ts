import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'status_instafeed';

  flag = true
  linhaVencido = 'red'
  linhaAtencao = 'yellow' 
  linhaNormal = 'white'
  
 displayedColumns: string[] = [ 'name', 'weight', 'symbol'];
 dataSource = ELEMENT_DATA;
}

export interface PeriodicElement {
  name: string;
  data: string;
  faltam: number;    
}


const ELEMENT_DATA: PeriodicElement[] = [
  { name: 'pe.de.cuba', data: '07/06/2022', faltam: diasVencimento('07/06/2022')}, 
  { name: 'mirakbrasil', data: '20/01/2022', faltam: diasVencimento('20/01/2022')},
  { name: 'abelharainhavirtual', data: '22/07/2022', faltam: diasVencimento('22/07/2022')},
  { name: 'lojadelayout', data: '03/06/2022', faltam: diasVencimento('03/06/2022')},
  { name: 'lolacosmetics', data: '12/07/2022', faltam: diasVencimento('12/07/2022')},   
  { name: 'momentopresente.gifts', data: '10/08/2021', faltam: diasVencimento('10/08/2021')},
  { name: 'mtsolucoes', data: '03/06/2022', faltam: diasVencimento('03/06/2022')},
  { name: 'otica.oficinotica', data: '02/04/2022', faltam: diasVencimento('02/04/2022')},
  { name: 'clubedofitness_outlet', data: '13/09/2021', faltam: diasVencimento('13/09/2021')},
  { name: 'senvolervelas', data: '04/07/2022', faltam: diasVencimento('07/07/2022')},
  { name: 'floopheadshop', data: '08/10/2022', faltam: diasVencimento('08/10/2022')},
  { name: 'loja_mundo_cristao_', data: '14/11/2021', faltam: diasVencimento('14/11/2021')},
  { name: 'sorellaoficial', data: '30/03/2022', faltam: diasVencimento('30/03/2022')},
  { name: 'shoprayza', data: '04/02/2022', faltam: diasVencimento('04/02/2022')},
  { name: 'paiolatacado', data: '22/06/2022', faltam: diasVencimento('22/06/2022')},
  { name: 'lojadalu_', data: '01/06/2022', faltam: diasVencimento('01/06/2022')},
  { name: 'tumor_osseo_reynaldojesus', data: '28/02/2022', faltam: diasVencimento('28/02/2022')},
  { name: 'petracoes.patos', data: '02/01/2022', faltam: diasVencimento('02/01/2022')},
  { name: 'bufalogrowler', data: '01/03/2022', faltam: diasVencimento('01/03/2022')},
  { name: 'amor.equestre', data: '22/06/2022', faltam: diasVencimento('22/06/2022')},
  { name: 'clubedofitness_outlet', data: '13/09/2021', faltam: diasVencimento('13/09/2021')},
  { name: 'senvolervelas', data: '04/07/2022', faltam: diasVencimento('07/07/2022')},
  { name: 'floopheadshop', data: '08/10/2022', faltam: diasVencimento('08/10/2022')},
  { name: 'loja_mundo_cristao_', data: '14/11/2021', faltam: diasVencimento('14/11/2021')},
  { name: 'lhd_perfis', data: '22/01/2022', faltam: diasVencimento('22/01/2022')},
  { name: 'aquilabaroni', data: '17/06/2022', faltam: diasVencimento('17/06/2022')},
  { name: 'revendananet', data: '20/06/2022', faltam: diasVencimento('20/06/2022')},
  { name: 'biluboutiquessa', data: '01/06/2022', faltam: diasVencimento('01/06/2022')},
  { name: 'rafamelhoresrp', data: '01/105/2022', faltam: diasVencimento('01/05/2022')},
  { name: 'floresbellabh', data: '04/04/2022', faltam: diasVencimento('04/04/2022')},
  { name: 'boutiquebycarlacarvalho', data: '06/06/2022', faltam: diasVencimento('06/06/2022')},
  { name: 'odontopan_oficial', data: '05/06/2022', faltam: diasVencimento('05/06/2022')},
  { name: 'viadoterno', data: '04/07/2022', faltam: diasVencimento('04/07/2022')},
  { name: 'zahara.decor', data: '17/06/2022', faltam: diasVencimento('17/06/2022')},
  { name: 'regen_import', data: '05/06/2022', faltam: diasVencimento('05/06/2022')},
  { name: 'larmodelar', data: '28/06/2022', faltam: diasVencimento('28/06/2022')},
  { name: 'noorjoias', data: '12/07/2022', faltam: diasVencimento('12/07/2022')},
  { name: 'fills.hair', data: '12/07/2022', faltam: diasVencimento('12/07/2022')},
  { name: 'dm4informatica', data: '12/07/2022', faltam: diasVencimento('12/07/2022')}  
];


function diasVencimento(dataVencimento:string){
    let data = dataVencimento.split('/').reverse().join('/');
    let falta = (new Date(data).getTime() - new Date().getTime()) / 1000;
    let dias = Math.round(falta / 60 / 60 / 24)+1; 
    return dias  
}